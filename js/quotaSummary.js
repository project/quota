/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document) {

  "use strict";

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.quotaSummary = {
    attach: function (context, settings) {

      // Update quota fieldset summary.
      $("fieldset#edit-quota", context).drupalSetSummary(function (context) {

        var quota = $("#edit-quota--2").val(),
            suffix,
            message,
            summary = "";

        // Generate summary if quota is set.
        if (quota) {
          suffix = quota > 1 ? "s" : "";
          message = "@quota";
          message += $("#edit-quota-unpublished").is(":checked") ? " unpublished" : "";
          message += " node" + suffix + ".";
          message += $("#edit-quota-warning").is(":checked") ? " Quota warning." : "";
          summary = Drupal.t(message, {"@quota": quota});
        }

        return summary;
      });
    }
  };

})(jQuery, Drupal, this, this.document);
