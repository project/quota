Quota module allows administrator to limit maximum number of nodes of each
content type a user can create. The module takes into account either all or
only unpublished nodes. This can be useful to limit a user activity until
his new nodes are moderated and published. If a user reaches the node creation
limit he can still access node add form page without access denied error. However
the warning message "Node creation disabled. You reached maximum number of
@type nodes — @quota." will be shown instead of a form.

Quota node limits can be set in Quota fieldset on a content type edit page -
Administration » Structure » Content types » Your Content type. Optionally
a warning message can be set on a node add form for users which didn't reach
the quota limit.

The module also defines a self-explanatory "bypass quota limit" permission.

